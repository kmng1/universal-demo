# UniversalDemo

## Build angular universal
Run `ng build --prod` to build the client.


Run `ng run universal-demo:server` to build the server.

## Run Express
Use `nodemon` or `node main.js`.

## Slides
[https://bit.ly/stackup_angular_universal](https://bit.ly/stackup_angular_universal)

## Angular vs React SSR
[A comparison of Server Side Rendering in React and Angular applications](https://hackernoon.com/a-comparison-of-server-side-rendering-in-react-and-angular-applications-fb95285fb716)
